using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace FlappyBird
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private float timeToReloadScene;

        [Space, SerializeField]
        private UnityEvent onGameOver;
        [SerializeField]
        private UnityEvent onIncreaseScore;

        [SerializeField]
        GameObject player, background;

        [SerializeField]
        Sprite background1, background2;

        [SerializeField]
        List<Pipe> pipes;


        public bool isGameOver { get; private set; }
        public int scoreCount { get; private set; }

        public static GameManager Instance
        {
            get; private set;
        }

        private void Awake()
        {
            if (Instance != null)
                DestroyImmediate(gameObject);
            else
                Instance = this;
        }

        private void OnEnable()
        {
            int index = Random.Range(0, 3);
            player.GetComponent<Bird>().ChangeBirdSkin(index);
            switch (index)
            {
                case 0:
                    background.GetComponent<SpriteRenderer>().sprite = background1;
                    break;

                case 1:
                    background.GetComponent<SpriteRenderer>().sprite = background2;
                    break;

                default:
                    background.GetComponent<SpriteRenderer>().sprite = background2;
                    break;
            }
            foreach (Pipe item in pipes)
            {
                item.ChangePipeSkin(index);
            }

        }

        public void GameOver()
        {
            Debug.Log("GameManager :: GameOver()");

            isGameOver = true;

            if (onGameOver != null)
                onGameOver.Invoke();

            StartCoroutine(ReloadScene());
        }

        public void IncreaseScore()
        {
            scoreCount++;

            if (onIncreaseScore != null)
                onIncreaseScore.Invoke();
        }
        private IEnumerator ReloadScene()
        {
            yield return new WaitForSeconds(timeToReloadScene);

            SceneManager.LoadScene(0);
        }
    }
}