using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlappyBird
{
    public class PipeSpawner : MonoBehaviour
    {
        public static PipeSpawner instance;

        private void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(this);
            }
            else instance = this;
        }

        [SerializeField]
        public Transform spawnPoint;

        [SerializeField] List<GameObject> pipesPool;
        [SerializeField] int currentPipe;
        public void SpawnPipe()
        {
            if (currentPipe < pipesPool.Count-1)
            {
                currentPipe++;
            }
            else currentPipe = 0;

            pipesPool[currentPipe].transform.position = new Vector2(spawnPoint.position.x, pipesPool[currentPipe].transform.position.y);
            pipesPool[currentPipe].SetActive(true);
        }
    }
}