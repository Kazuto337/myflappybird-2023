using System.Collections;
using UnityEngine;

namespace FlappyBird
{
    public class Pipe : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float timeToDestroyPipe;
        [SerializeField]
        GameObject topPipeObject, botPipeObject;
        [SerializeField]
        Sprite topPipe1, topPipe2, botPipe1, botPipe2;

        private void OnEnable()
        {
            StartCoroutine(DestroyPipe());
        }

        private void Update()
        {
            if (GameManager.Instance.isGameOver)
                return;

            transform.position += (Vector3.left * Time.deltaTime * speed);
        }

        private IEnumerator DestroyPipe()
        {
            yield return new WaitForSeconds(timeToDestroyPipe);
            PipeSpawner.instance.SpawnPipe();
            transform.position = new Vector2(PipeSpawner.instance.spawnPoint.position.x , transform.position.y );
            gameObject.SetActive(false);
        }

        public void ChangePipeSkin(int skinIndex)
        {
            switch (skinIndex)
            {
                case 0:
                    topPipeObject.GetComponent<SpriteRenderer>().sprite = topPipe1;
                    botPipeObject.GetComponent<SpriteRenderer>().sprite = botPipe1;
                    break;
                case 1:
                    topPipeObject.GetComponent<SpriteRenderer>().sprite = topPipe2;
                    botPipeObject.GetComponent<SpriteRenderer>().sprite = botPipe2;
                    break;
                default:
                    topPipeObject.GetComponent<SpriteRenderer>().sprite = topPipe1;
                    botPipeObject.GetComponent<SpriteRenderer>().sprite = botPipe1;
                    break;
            }
        }
    }
}